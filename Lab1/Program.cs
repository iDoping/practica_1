﻿using System;

namespace Lab1
{
    class Program
    {
        static void Main(string[] args)
        {
            #region 1 способ
            ////while (true)
            ////{
            ////    Console.Write("Введите два числа через пробел: ");
            ////    var input = Console.ReadLine();
            ////    if (input.ToLower() == "exit")
            ////        break;
            ////    var mass = input.Split(' ');
            ////    Console.WriteLine($"Результат сложения: {Double.Parse(mass[0]) + Double.Parse(mass[1])}");
            ////}
            #endregion
            #region 2 способ
            Console.WriteLine("Введите два числа: ");
            double a = Double.Parse(Console.ReadLine());
            double b = Double.Parse(Console.ReadLine());
            Console.WriteLine($"Результат сложения: {a + b}");
            #endregion
        }
    }
}