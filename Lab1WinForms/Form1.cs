﻿using System;
using System.Windows.Forms;

namespace Lab1WinForms
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void doSum_Click(object sender, EventArgs e)
        {
            result.Text = Result(number1.Text, number2.Text);
        }

        private string Result(string n1, string n2)
        {
            return (Double.Parse(n1) + Double.Parse(n2)).ToString();
        }
    }
}